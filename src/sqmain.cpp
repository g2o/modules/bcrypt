#include <sqapi.h>
#include "bcrypt.h"

SQInteger sq_bcrypt_genSalt(HSQUIRRELVM vm)
{
	SQInteger top = sq_gettop(vm);
	if (top > 3)
		return sq_throwerror(vm, "wrong number of parameters");

	SQInteger rounds = 10;
	if (top >= 2)
		sq_getinteger(vm, 2, &rounds);

	SQInteger minor = 'b';
	if (top == 3)
		sq_getinteger(vm, 3, &minor);

	std::string salt = bcrypt::generateSalt(rounds, minor);
	sq_pushstring(vm, salt.c_str(), -1);

	return 1;
}

std::string bcrypt_hash(const char* data, const char* salt)
{
	return bcrypt::generateHash(data, salt);
}

bool bcrypt_compare(const char* data, const char* hash)
{
	return bcrypt::validatePassword(data, hash);
}

int bcrypt_getRounds(const char* hash)
{
	return bcrypt::getRounds(hash);
}

extern "C" SQRESULT SQRAT_API sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
	SqModule::Initialize(vm, api);

	 Sqrat::RootTable roottable(vm);

	/* squirreldoc (func)
	*
	* This function generates a salt.
	*
	* @side		shared
	* @name		bcrypt_genSalt
	* @param	(int) rounds=10 the numer of rounds that the algorithm will use, can't be lower than `4` or higher than `31`.
	* @param	(int) minor='b' the minor character used in generating bcrypt salt, it will be the 3rd character of the generated salt.
	* @return	(string) the generated salt string for bcrypt.
	*
	*/
	 roottable.SquirrelFunc("bcrypt_genSalt", sq_bcrypt_genSalt, -1, ".ii");

	/* squirreldoc (func)
	*
	* This function hashes an input string with the usage of generated salt.
	*
	* @side		shared
	* @name		bcrypt_hash
	* @param	(string) data the data string that you want to hash.
	* @param	(string) salt the generated salt.
	* @return	(string) the hashed text.
	*
	*/
	 roottable.Func("bcrypt_hash", bcrypt_hash);

	/* squirreldoc (func)
	*
	* This function checkes if the passed string will produce the same hash as the passed one.
	*
	* @side		shared
	* @name		bcrypt_compare
	* @param	(string) data the data string that you want to check.
	* @param	(string) hash the generated hash that you want to compare with data.
	* @return	(bool) `true` if the hash matches the data text, otherwise `false`.
	*
	*/
	 roottable.Func("bcrypt_compare", bcrypt_compare);

	/* squirreldoc (func)
	*
	* This function retrieves the number of rounds used to encrypt a given hash.
	*
	* @side		shared
	* @name		bcrypt_getRounds
	* @param	(string) hash the generated hash that you want to read rounds from.
	* @return	(int) the number of roudns that was used to generate hash.
	*
	*/
	 roottable.Func("bcrypt_getRounds", bcrypt_getRounds);

	return SQ_OK;
}
