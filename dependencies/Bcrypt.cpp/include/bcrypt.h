#ifndef BCRYPT_H
#define BCRYPT_H

#include <string>

namespace bcrypt {

    std::string generateSalt(unsigned rounds = 10, char minor = 'b');
    std::string generateHash(const std::string& password, const std::string& salt);
    bool validatePassword(const std::string& password, const std::string& hash);
    unsigned getRounds(const char* hash);
}

#endif // BCRYPT_H
