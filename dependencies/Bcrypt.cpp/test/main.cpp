#include "bcrypt.h"
#include <iostream>
#include <string>


int main()
{

    std::string password = "CÊVZAíºÊ¤É6îÎ6Jo*¤ÛØ×<¶Bµ¯u³õi\"%M";

    std::string hash = "$2y$10$dPiUIVzwJ4yTOJKoCSoHwOaHD8KVy9vVnp24156e0Nn7gvzFmDWpm";

    std::cout << "Hash: " << hash << std::endl;

    std::cout << "\"" << password << "\" : " << bcrypt::validatePassword(password,hash) << std::endl;
    std::cout << "\"wrong\" : " << bcrypt::validatePassword("wrong",hash) << std::endl;

    return 0;
}
