# bcrypt

## Introduction

bcrypt module allows you to work with bcrypt hash algorithm.  
It can be very useful when you're working with some external database that holds hashed text using this algorithm.  
Module supports `'a'`, `'b'`, `'x'`, `'y'`, minors for generating salt (x & y are treated the same as a & b).

The project uses modified source code as a base taken from here: https://github.com/hilch/Bcrypt.cpp  
The project main inspiration was: https://www.npmjs.com/package/bcrypt

## Documentation

https://g2o.gitlab.io/modules/bcrypt